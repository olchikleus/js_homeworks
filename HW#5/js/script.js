"use strict"

// Теоретичні питання
// 1) Опишіть своїми словами, що таке метод об'єкту
//        Метод об'єкту це властивості, значення яких є функцією. Використовуются для роботи з властивостями об'єкту всередині об'єкта.
// 2) Який тип даних може мати значення властивості об'єкта?
//       будь-яких тип данних.
// 3) Об'єкт це посилальний тип даних. Що означає це поняття?
//      Складне питання. Я прочитала по ньому статтю https://uk.javascript.info/reference-type , але самостійно сформулювати відповідь на питання складно.



function createNewUser (firstName, lastName) {
    const newUser ={
        firstName,
        lastName,
        getLogin: function (){
            return `${newUser.firstName[0]}${newUser.lastName}`
        }
    }
    return newUser;
}

let userName  = prompt("Enter your name");
let userSurname  = prompt("Enter your surname");

console.log(createNewUser(`${userName}`, `${userSurname}`))

// console.log((createNewUser (`${userName}`, `${userSurname}`).getLogin()).toLowerCase())



