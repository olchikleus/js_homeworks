"use strict"

// Теоретичні питання
// 1)Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//       Якщо в строке є спеціальні символи (наприклад,  \ ^ $  * + ( ) [ ] . '') нам перед такими символами треба додати зворотній слеш (\). для того щоб програма прочитала їх як текст, а не сприймала як частину коду.
// 2)Які засоби оголошення функцій ви знаєте?
//    Function Declaration.
//    Function Expression.
// 3)Що таке hoisting, як він працює для змінних та функцій?
//      Це механізм JS в якому змінні та функції, піднімаються вверх своєї області видимості перед тим, як код буде виконано.
//      Тобто, не важливо в якому місці ми оголосили функцію чи змінну (в локальній чи глобальній області видимості), вони все одно піднімаються вверх своєї області видимості.

function createNewUser (firstName, lastName, birthday) {
    const newUser ={
        firstName,
        lastName,
        birthday,
        getLogin: function (){
            return `${newUser.firstName[0]}${newUser.lastName}`
        },
        getPassword: function (){
            return `${newUser.firstName[0]}${newUser.lastName}${newUser.birthday}`
        },
        getAge: function (){
            const [day, month, year] = userBirth.split(".")
            let now = new Date();
            let today = new Date(now.getFullYear(), now.getMonth(), now.getDate())
            let birthDate  = new Date(year, month-1, day)
            let birthDateNow = new Date(today.getFullYear(), birthDate.getMonth(), birthDate.getDate())
            let age
            age = now.getFullYear() - birthDate.getFullYear()
            if (today < birthDateNow){
                age = age - 1
            }

            return age
        }

    }
    return newUser
}

let userName  = prompt("Enter your name")
let userSurname  = prompt("Enter your surname")
let userBirth = prompt("Enter your date of birth", "dd.mm.yyyy")
console.log((createNewUser (userName, userSurname, userBirth)))
console.log((createNewUser (userName, userSurname).getLogin()).toLowerCase())
console.log(createNewUser(userName.toUpperCase(), userSurname.toLowerCase(), userBirth.slice(6)).getPassword())
console.log(createNewUser(userName.toUpperCase(), userSurname.toLowerCase(), userBirth.slice(6)).getAge())
