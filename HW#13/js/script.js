"use strict"


let images = document.querySelectorAll('.images-wrapper .image');
let currentImage = 0;
let imageInterval = setInterval(viewImages,3000);



function viewImages(){
    images[currentImage].classList = "image" ;
    currentImage = (currentImage + 1) % images.length;
    images[currentImage].classList = "image image-to-show";
}


const stopBtn = document.createElement("button");
document.body.prepend(stopBtn);
stopBtn.innerText = "Припинити";
stopBtn.style.fontSize =  "20px";
stopBtn.style.backgroundColor = "green";
stopBtn.style.color = "yellow";
stopBtn.style.height = "40px";
stopBtn.style.border= "1px solid green";
stopBtn.style.margin= "20px";
stopBtn.style.cursor= "pointer";

const startBtn = document.createElement("button");
stopBtn.after(startBtn);
startBtn.innerText = "Відновити показ";
startBtn.style.fontSize =  "20px";
startBtn.style.backgroundColor = "green";
startBtn.style.color = "yellow";
startBtn.style.height = "40px";
startBtn.style.border= "1px solid green";
startBtn.style.cursor= "pointer";
startBtn.style.margin= "20px";

let playing = true;

stopBtn.addEventListener("click",()=>{
    function stopViewImages() {
        playing = false;
        clearInterval(imageInterval);
    }
    stopViewImages()
})

startBtn.addEventListener("click",()=>{
    function startViewImages() {
        playing = true;
        imageInterval = setInterval(viewImages,3000)
    }
    startViewImages()
})






