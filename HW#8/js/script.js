"use strict"

// Теоретичні питання
// 1) Опишіть своїми словами що таке Document Object Model (DOM).
//       DOM - це об'єктна модель документа, яка надає весь вміст сторінки у вигляді об'єктів, які можна змінювати.
// 2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//        innerText - повертає тільки текст, який знаходиться в тегу.
//        innerHTML - повертає текст і сам тег.
// 3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//           .getElementById ()
//           .getElementsByClassName()
//           .getElementsByTagName()
//           .getElementsByName()
//           .querySelectorAll()
//           .querySelector()
//           Перші чотири методи - застарілі, які зараз зустрічаються в старих скриптах або в деяких випадках можуть і зараз бути корисними. А останні два найпопулярніші і вони можуть замінити застарілі методи. Тому зараз використовують переважно querySelectorAll()
//     та querySelector().


// 1)Знайти всі параграфи на сторінці та встановити колір фону #ff0000
    let paragraph = document.querySelectorAll("p");
    for (let text of paragraph){
    // console.log(text.innerText)
    text.style.color = "#ff0000"
}

// 2)Знайти елемент із id="optionsList".Вивести у консоль.
//       let id = document.querySelector("#optionsList");
//       console.log(id);
   // Знайти батьківський елемент та вивести в консоль.
   //       let id = document.querySelector("#optionsList").parentNode;
   //       console.log(id);
  // Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
  //        let id = document.querySelector("#optionsList");
  //         let nodes = id.childNodes
  //        console.log(nodes);
  //         for (let node of nodes){
  //             console.log(node.nodeType);
  //             console.log(node.textContent)
  //         }

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph.
// document.getElementById("testParagraph").innerHTML = "This is a paragraph"

// 4-5) Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
//     Кожному з елементів присвоїти новий клас nav-item.

// let allElements = document.querySelectorAll(".main-header");
//        for (let node of allElements){
//        console.log(node)
//        }

// let elements = document.querySelectorAll(".main-header > div");
// console.log(elements)
//        for (let items of elements) {
//            console.log(items);
//            items.classList.add("nav-item")
//        }

// 6) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
//     let sectionTitle = document.querySelectorAll(".section-title");
//     for (let text of sectionTitle){
//         text.classList.remove("section-title")
//     }









