"use strict"
// Теоретичні питання
//
// 1) Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
//     Цикли забезпечують виконання однотипних операцій, які можуть багато разів повторюватсь при написанні програми.
// 2) Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
//        Цикл for - у випадку, якщо умовою задачі передбачено виконнання будь-якої дії певну кількість разів (наприклад, 5 або 6 разів і т.д.)
//        Цикл while - Питаємо у користувача його вік.Задаємо умову (наприклад, для отримання доступу на сайт вік користувача має бути не менше 16 років). Якщо результат буде true (користувач зазначить вік більше 16 років) цикл виконується і йдемо далі. Якщо вік не відповідає зазначеній умові - продовжуємо запитувати, поки результат не буде true.
//        Цикл do_while - Питаємо у користувача його вік. Задаємо умову (наприклад, для отримання доступу на сайт вік користувача має бути не менше 16 років). Якщо результат буде true (користувач зазначить вік більше 16 років) цикл виконується і йдемо далі.. В іншому випадку цикл приривається і
// завершується.
// 3) Що таке явне та неявне приведення (перетворення) типів даних у JS?
//     Явне перетворення типів - навмисне перетворення типів за допомогою функцій "String" , "Boolean" , "Number".
//     Неявне - перетворення типів за допомогою:
//      - бінарного оператора(+): при приведенні до строкового типу;
//      - логічних опеаторів (||, &&): при переведенні в булевий тип;
//      -арифметичних операторів (+,-,*, /), операторів порівняння (<, >, <=, =>) і т.д.: при числовому перетворенні.



// let userNumber = +prompt("Enter any number.");
// while (!Number.isInteger (userNumber))
//     userNumber = +prompt("Enter integer number, please.");
// for(let i=1; i<=userNumber; i++){
//     if (userNumber >=1 && userNumber <= 4) {
//         console.log("Sorry, no numbers");
//     }else
//         if(i % 5 === 0){
//             console.log(i);
//         }
// }

let userNumber = +prompt("Enter any number.");
while (!Number.isInteger (userNumber))
    userNumber = +prompt("Enter integer number, please.");
if(userNumber >=0 && userNumber <= 4){
    console.log("Sorry, no numbers");
}
for(let i=0; i<=userNumber; i++){
    if (i % 5 === 0) {
        console.log (i);
    }
}



// Необов'язкове завдання

// let userNumber1 = +prompt("Enter any number1");
// let userNumber2 = +prompt("Enter any number2");
//  while (isNaN(userNumber1) || isNaN(userNumber2)){
//       userNumber1 = +prompt("Enter any number1");
//       userNumber2 = +prompt("Enter any number2");
//  }
//  for (let i = userNumber1; userNumber1 <= userNumber2; userNumber1++){
//      for (let j=2; j< userNumber1; j++){
//          if (userNumber1 % j === 0){
//              break;
//          }else if (j  === userNumber1-1){
//              console.log(userNumber1)
//          }
//      }
//  }




